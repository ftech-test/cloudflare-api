package util

import (
	"fmt"
	"os"
	"testing"
)

// setEnvVars sets the necessary environment variables for testing
func setEnvVars() {
	os.Setenv("ZONEID_CF", "zone123,zone456")
	os.Setenv("TOKEN_CF", "dummy_token")
	os.Setenv("AUTH_EMAIL", "test@example.com")
	os.Setenv("DOMAIN", "example.com,example.org")
	os.Setenv("USERNAME", "testuser")
	os.Setenv("PASSWORD", "testpassword")
	os.Setenv("API_ADDRESS", "api1.example.com,api2.example.com")
	os.Setenv("RESOURCE_ID", "123,456")
}

func TestPrintCDNs(t *testing.T) {
	// Set environment variables
	setEnvVars()

	// Initialize the package by calling init function implicitly
	// The init function is automatically called when the package is imported

	// Print CDNs
	cdns := GetCDNs()
	for _, cdn := range cdns {
		fmt.Printf("API Address: %s, Resource ID: %d\n", cdn.ApiAddress, cdn.ResourceID)
	}
}
