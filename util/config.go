package util

import (
	"cloudflare-api/model"
	"log"
	"os"
	"strconv"
	"strings"
)

// Vendor
var UserName string
var PassWord string
var ApiAddress string
var ResourceID string
var CDNs []model.CDN

// CloudFlare
var ZoneId string
var TokenCF string
var AuthEmail string
var DomainCF string
var Domains []model.SiteFtech

func intConvert(s string) int {
	x, err := strconv.Atoi(s)
	if err != nil {
		log.Fatal(err)
	}

	return x
}

func init() {
	tokenArray := []string{"token1", "token2", "token3"}
	resourceArray := []string{"111", "222", "333"}
	apiAddressArray := []string{"add1", "add2", "add3"}


	ZoneId = os.Getenv("ZONEID_CF")
	// TokenCF = "Bearer " + os.Getenv("TOKEN_CF")
	AuthEmail = os.Getenv("AUTH_EMAIL")
	DomainCF = os.Getenv("DOMAIN")

	UserName = os.Getenv("USERNAME")
	PassWord = os.Getenv("PASSWORD")

	var domainCF = strings.Split(DomainCF, ",")
	var zoneIds = strings.Split(ZoneId, ",")
	for i, domain := range domainCF {
		var tmp = model.SiteFtech{
			Domain: domain,
			ZoneId: zoneIds[i],
		}
		Domains = append(Domains, tmp)
	}

	var apiAddresses = strings.Split(strings.Join(apiAddressArray, ","), ",")
	var resourceIds = strings.Split(strings.Join(resourceArray, ","), ",")

	for i, apiAddress := range apiAddresses {
		var tmp = model.CDN{
			ApiAddress: apiAddress,
			ResourceID: intConvert(resourceIds[i]),
			Token:      tokenArray[i],
		}
		CDNs = append(CDNs, tmp)
	}

}
