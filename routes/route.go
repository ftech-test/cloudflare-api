package routes

import (
	"net/http"

	"cloudflare-api/entities"

	"github.com/gin-gonic/gin"
)

func Config() *gin.Engine {
	r := gin.Default()
	r.Use(gin.Recovery())
	homepage := r.Group("")
	{
		homepage.GET("", func(c *gin.Context) {
			c.JSON(http.StatusOK, "OK")
		})
	}
	backend := r.Group("api")
	{
		backend.GET("/health", func(c *gin.Context) {
			c.JSON(http.StatusOK, "OK")
		})
		backend.POST("", entities.Auth())

	}
	return r
}
