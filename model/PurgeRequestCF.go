package model

type PurgeRequestCF struct {
	Files []PurgePathCF `json:"files"`
}

type PurgePathCF struct {
	Url string `json:"url"`
}

type SiteFtech struct {
	Domain string `json:"domain"`
	ZoneId string `json:"zone_id"`
}
