package model

import gormjsonb "github.com/dariubs/gorm-jsonb"

type PurgeResponse struct {
	Notice gormjsonb.JSONB `json:"is_noti"`
}
