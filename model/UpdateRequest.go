package model

type UpdateRequest struct {
	PurgeRequest
	ResourceID int    `json:"resource_id" validate:"required"`
	Vendor     string `json:"vendor" validate:"required"`
	Domain     string `json:"domain" validate:"required"`
}
