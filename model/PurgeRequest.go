package model

type PurgeRequest struct {
	PurgePath string `json:"purge_paths" validate:"required"`
}

type CDN struct {
	ApiAddress string
	ResourceID int
	Token      string
}
