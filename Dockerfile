FROM golang:1.21.6-alpine3.19 as build-env

WORKDIR /app

COPY go.mod /go.sum /app/
RUN go mod download

COPY . /app/

RUN CGO_ENABLED=0 go build -o /cloudflare-api


FROM alpine


COPY --from=build-env /cloudflare-api /usr/local/bin/cloudflare-api
RUN chmod +x /usr/local/bin/cloudflare-api


ENTRYPOINT ["cloudflare-api"]