package main

import (
	"cloudflare-api/routes"
	"cloudflare-api/util"
	"fmt"
)

func main() {
	fmt.Println("Updated CDNs:", util.CDNs)
	r := routes.Config()
	r.Run(":8085")
}
