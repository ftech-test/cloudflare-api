package entities

import (
	"cloudflare-api/model"
	"cloudflare-api/util"
	"encoding/json"
	"log"

	"github.com/gin-gonic/gin"
)

func Vendor(ctx *gin.Context) model.PurgeResponse {
	var requestBody model.UpdateRequest
	var resp model.PurgeResponse

	if err := ctx.BindJSON(&requestBody); err != nil {
		log.Fatal(err)
	}

	if requestBody.Vendor == "cloudflare" {
		var zoneid string
		for i := range util.Domains {
			if requestBody.Domain == util.Domains[i].Domain {
				zoneid = util.Domains[i].ZoneId
			}
		}

		if zoneid == "" {
			err := json.Unmarshal([]byte(`{"message": "domain not found! Please contact IS Team"}`), &resp.Notice)
			if err != nil {
				log.Println(err)
			}
		} else {
			resp = PurgeCacheCF(ctx, zoneid)
		}
	} else {
		var cdn_idx int
		var cdn_exits = 0
		for i := range util.CDNs {
			if requestBody.ResourceID == util.CDNs[i].ResourceID {
				cdn_idx = i
				cdn_exits = 1
			}
		}
		if cdn_exits == 0 {
			err := json.Unmarshal([]byte(`{"message": "resourceID for CDN not found! Please contact IS Team"}`), &resp.Notice)
			if err != nil {
				log.Println(err)
			}
		} else {
			resp = PurgeCache(ctx, cdn_idx, requestBody.PurgePath)
		}
	}

	return resp
}
