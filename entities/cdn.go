package entities

import (
	"bytes"
	"cloudflare-api/model"
	"cloudflare-api/util"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func PurgeCache(ctx *gin.Context, cdn_idx int, purgePath string) model.PurgeResponse {
	var purgeBody model.PurgeRequest

	requestURL := fmt.Sprintf("https://%s/cdn_resources/%d/purge", util.CDNs[cdn_idx].ApiAddress, util.CDNs[cdn_idx].ResourceID)
	purgeBody.PurgePath = purgePath

	log.Printf("Purge Cache for CDN %s", util.CDNs[cdn_idx].ApiAddress)
	jsonBody, err := json.Marshal(purgeBody)
	if err != nil {
		log.Println(err)
	}

	bodyReader := bytes.NewReader(jsonBody)
	req, err := http.NewRequest(http.MethodPost, requestURL, bodyReader)
	if err != nil {
		log.Printf("client: could not create request: %s\n", err)
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")

	req.SetBasicAuth(util.UserName, util.PassWord)

	client := http.Client{
		Timeout: 30 * time.Second,
	}

	res, err := client.Do(req)
	if err != nil {
		log.Printf("client: error making http request: %s\n", err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("Error reading response body:", err)
	}
	var noti model.PurgeResponse
	err = json.Unmarshal(body, &noti.Notice)
	if err != nil {
		log.Println(err)
	}

	return noti
}
