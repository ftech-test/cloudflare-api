package entities

import (
	"fmt"
)

// checkArrayLengths verifies that the lengths of tokenArray and resourceArray are equal.
func checkArrayLengths(tokenArray, resourceArray []string) bool {
	return len(tokenArray) == len(resourceArray)
}

// createTokenResourceMap creates a map from tokens to resources if their lengths are equal.
func createTokenResourceMap(tokenArray, resourceArray []string) map[string]string {
	// Check if the lengths of tokenArray and resourceArray are equal
	if !checkArrayLengths(tokenArray, resourceArray) {
		fmt.Println("tokenArray and resourceArray have different lengths")
		return nil
	}

	tokenResourceMap := make(map[string]string)

	// Create the map of token to resource
	for i := 0; i < len(tokenArray); i++ {
		token := tokenArray[i]
		resource := resourceArray[i]
		tokenResourceMap[token] = resource
	}
	return tokenResourceMap
}