package entities

import (
	"cloudflare-api/model"
	"fmt"
	"net/http"
	"strings"
	// "log"
	"github.com/gin-gonic/gin"
)

type UpdateRequest struct {
	ResourceID string `json:"resource_id"`
}
type PurgeRequest struct {
	PurgePaths string `json:"purge_paths"`
	ResourceID string `json:"resource_id"`
}
func HandleAPIRequest(ctx *gin.Context) string {
	var requestBody UpdateRequest
	if err := ctx.BindJSON(&requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return ""
	}
	return requestBody.ResourceID
}

func Auth() gin.HandlerFunc {
	var resp model.PurgeResponse
	tokenArray := []string{"9CPkOxTpkItgfQx52ZA6gNqnxdiclXw8xpKpiQU9", "token2", "token3"}
	resourceArray := []string{"383", "222", "333"}
	tokenResourceMap := createTokenResourceMap(tokenArray, resourceArray)

	if tokenResourceMap == nil {
		return func(ctx *gin.Context) {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": "tokenArray and resourceArray have different lengths"})
			ctx.Abort()
		}
	}
	return func(ctx *gin.Context) {
		bearerToken := ctx.Request.Header.Get("Authorization")

		if bearerToken == "" {
			ctx.JSON(http.StatusUnauthorized, gin.H{
				"message": "Unauthorized: token not provided",
			})
			ctx.Abort()
			return
		}
		
		reqToken := strings.Split(bearerToken, " ")[1]

		if resource, ok := tokenResourceMap[reqToken]; ok {
			resourceId_User := HandleAPIRequest(ctx)
			message := fmt.Sprintf("resourceId_User = %s, resource = %s", resourceId_User, resource)
			ctx.JSON(http.StatusUnauthorized, gin.H{
				"message": message,
			})
			if resourceId_User == resource {
				// message := "Resource IDs are match"
				// ctx.JSON(http.StatusUnauthorized, gin.H{
				// 	"message": message,
				// })
				resp = Vendor(ctx)
				ctx.IndentedJSON(http.StatusOK, resp.Notice)
			} else {
				message := "Resource IDs are not match"
				ctx.JSON(http.StatusUnauthorized, gin.H{
					"message": message,
				})
			}
		} else {
			ctx.JSON(http.StatusUnauthorized, gin.H{
				"message": "Unauthorized: invalid token",
			})
		}
		ctx.Abort()
	}
}
