package entities

import (
	"cloudflare-api/model"
	"cloudflare-api/util"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

const DefaultEndpoint = "https://api.cloudflare.com/"

const cacheEndpointFormat = "client/v4/zones/%s/purge_cache"

func PurgeCacheCF(ctx *gin.Context, zoneID string) model.PurgeResponse {

	requestURL := fmt.Sprintf(DefaultEndpoint+cacheEndpointFormat, zoneID)

	req, err := http.NewRequest(http.MethodPost, requestURL, strings.NewReader(`{"purge_everything":true}`))
	if err != nil {
		log.Printf("client: could not create request: %s\n", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", util.TokenCF)

	client := http.Client{
		Timeout: 30 * time.Second,
	}
	res, err := client.Do(req)
	if err != nil {
		log.Printf("client: error making http request: %s\n", err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("Error reading response body:", err)
	}
	var noti model.PurgeResponse
	err = json.Unmarshal(body, &noti.Notice)
	if err != nil {
		log.Println(err)
	}
	return noti
}
